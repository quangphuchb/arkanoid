﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameController : MonoBehaviour {
    public static int count;
    public GameObject OB;
    public Text txt;
    public GameObject ball;
    public GameObject replay;
    // Use this for initialization
    void Start() {
        count = OB.transform.childCount;

    }

    // Update is called once per frame
    void Update() {
        if (count <= 0)
        {
            txt.text = "Win";
            replay.SetActive(true);
        }
        if (!ball)
        {
            txt.text = "Lose";
            replay.SetActive(true);
        }
    }
    public void btnReplay()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
